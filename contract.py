# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta

type_of_employee = [
    ('01', 'Dependiente'),
    ('02', 'Servicio domestico'),
    ('04', 'Madre comunitaria'),
    ('12', 'Aprendices del Sena en etapa lectiva'),
    ('18', 'Funcionarios públicos sin tope máximo de ibc'),
    ('19', 'Aprendices del SENA en etapa productiva'),
    ('21', 'Estudiantes de postgrado en salud'),
    ('22', 'Profesor de establecimiento particular'),
    ('23', 'Estudiantes aportes solo riesgos laborales'),
    ('30', 'Dependiente entidades o universidades públicas con régimen especial en salud'),
    ('31', 'Cooperados o pre cooperativas de trabajo asociado'),
    ('47', 'Trabajador dependiente de entidad beneficiaria del sistema general de participaciones ‐ aportes patronales'),
    ('51', 'Trabajador de tiempo parcial'),
    ('54', 'Pre pensionado de entidad en liquidación.'),
    ('56', 'Pre pensionado con aporte voluntario a salud'),
    ('58', 'Estudiantes de prácticas laborales en el sector público')
]

subtype_of_employee = [
    ('00', 'No Aplica'),
    ('01', 'Dependiente pensionado por vejez activo')
]


class Contract(metaclass=PoolMeta):
    __name__ = 'staff.contract'

    type_of_employee = fields.Selection(type_of_employee, 
        'Type of Employee', states={'required':True})
    subtype_of_employee = fields.Selection(
        subtype_of_employee, 'SubType of Employee')

    def default_subtype_of_employee():
        return '00'
