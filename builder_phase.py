
import os
from decimal import Decimal
from lxml import etree

from .maker_phase import element
from .constants import EXTRAS, KIND, TIPO_INCAPACIDAD, WAGE_TYPE, MESSAGES

path = os.path.dirname(__file__)


def rvalue(value, n):
    return str(round(abs(value), n))


class ElectronicPayroll(object):

    def __init__(self, payroll, config):
        company = payroll.company.party
        party_employee = payroll.employee.party
        contract = payroll.contract

        self.payroll = payroll
        self.config = config
        self.status = 'ok'
        self.number = payroll.number
        # Company information --------------------------------------------------
        self.company_id = company.id_number
        self.company_type_id = company.type_document
        self.company_full_name = company.name
        self.company_first_name = company.first_name or None
        self.company_second_name = company.second_name or ''
        self.company_first_familyname = company.first_family_name or ''
        self.company_second_familyname = company.second_family_name or ''
        self.company_check_digit = company.check_digit
        self.company_country_code = 'CO'
        self.company_department_code = company.department_code
        self.company_city_code = company.city_code
        self.company_address = company.street.replace('\n', '')
        # Employee information ------------------------------------------------
        self.party_name = party_employee.name
        self.party_id = party_employee.id_number
        self.party_type_id = party_employee.type_document
        self.type_employee = contract.type_of_employee
        self.subtype_employee = contract.subtype_of_employee
        self.alto_riesgo_pension = contract.high_pension_risk or 'false'
        self.employee_first_name = party_employee.first_name
        self.employee_second_name = party_employee.second_name or ''
        self.employee_first_family_name = party_employee.first_family_name
        self.employee_second_family_name = party_employee.second_family_name or ''
        self.work_place_country = 'CO'
        if contract.workInstalationClient:
            self.work_place_department = self.company_department_code
            self.work_place_city = self.company_city_code
            self.work_place_address = self.company_address
        else:
            self.work_place_department = contract.subdivision_activity.code
            self.work_place_city = contract.city_activity.code
            self.work_place_address = contract.address_activity
        self.integral_salary = contract.integral_salary or 'false'
        self.type_contract = KIND[contract.kind]
        self.salary = contract.salary
        self.code_employee = payroll.employee.code or None
        self.payment_term = contract.payment_term
        self.payment_method = payroll.payment_method
        if payroll.bank_payment and party_employee.bank_accounts:
            self.bank = party_employee.bank_name or None
            self.bank_account_type = party_employee.bank_account_type or None
            self.bank_account = party_employee.bank_account or None

        self.currency = payroll.currency.code
        self.issue_date, self.issue_time = payroll.get_datetime_local()

        self.environment = config.environment
        self.period_payroll = config.period_payroll
        self.prefix = config.payroll_electronic_sequence.prefix
        self.software_id = config.software_id
        self.software_pin = config.pin_software
        self.payroll_type = self.payroll.payroll_type
        self.validate_payroll()

    def validate_payroll(self):
        for k in MESSAGES.keys():

            field_value = getattr(self, k)
            bank_inf = ['bank', 'bank_account_type', 'bank_account']
            if k in bank_inf and not self.payroll.bank_payment:
                continue
            elif not field_value and k != 'company_check_digit':
                self.status = MESSAGES[k]
                break
            elif k == 'company_check_digit' and not str(field_value).isdigit():
                self.status = MESSAGES[k]
                break

    def _get_head_psk(self):
        NomInd = open(path + '/Nomina_Individual_Electronica_V1.0.xml', 'r')
        root = etree.parse(NomInd).getroot()
        return root

    def _get_credit_head_psk(self):
        NomIndA = open(
            path + '/Nomina_Individual_Ajuste_Electronica_V1.0.xml', 'r')
        root = etree.parse(NomIndA).getroot()
        return root

    def _get_payroll_period(self):

        start_date = self.payroll.contract.start_date
        end_date, settlement_start_date, settlement_end_date = None, None, None
        if self.payroll.contract.finished_date and self.payroll.contract.finished_date <= self.payroll.end:
            settlement_end_date = self.payroll.contract.finished_date
            end_date = settlement_end_date

        for p in self.payroll.payrolls_relationship:
            if not settlement_start_date:
                settlement_start_date = p.start
            elif settlement_start_date <= p.start:
                settlement_start_date = p.start

            if not settlement_end_date:
                settlement_end_date = p.end
            elif settlement_end_date >= p.end:
                settlement_end_date = p.end
        if not settlement_start_date:
            settlement_start_date = self.payroll.start
        if not settlement_end_date or settlement_end_date < settlement_start_date:
            settlement_end_date = self.payroll.end
        payroll_period = element.Periodo(
            FechaIngreso=str(start_date),
            FechaLiquidacionInicio=str(settlement_start_date),
            FechaLiquidacionFin=str(settlement_end_date),
            TiempoLaborado=str(self.payroll.get_time_worked()),
            FechaGen=self.issue_date,
        )
        if end_date:
            payroll_period.set('FechaRetiro', str(end_date)),
        return payroll_period

    def _get_sequence(self):
        seq = self.number.split(self.prefix, maxsplit=1)
        sequence = element.NumeroSecuenciaXML(
            Prefijo=self.prefix,
            Consecutivo=seq[1],
            Numero=self.number,
        )
        if self.code_employee:
            pass
            # sequence.set('CodigoTrabajador', self.code_employee),
        return sequence

    def _get_place_generation(self):
        place_generation = element.LugarGeneracionXML(
            Pais=self.company_country_code,
            DepartamentoEstado=self.company_department_code,
            MunicipioCiudad=str(
                self.company_department_code + self.company_city_code),
            Idioma="es",
        )
        return place_generation

    def _get_provider(self):
        provider = element.ProveedorXML(
            RazonSocial=self.company_full_name,
            NIT=str(self.company_id),
            DV=str(self.company_check_digit),
            SoftwareID=self.software_id,
            SoftwareSC=self.payroll.get_security_code(self.config),
        )
        if self.company_first_name:
            provider.set('PrimerApellido', self.company_first_name)
            provider.set('SegundoApellido', self.company_second_familyname)
            provider.set('PrimerNombre', self.company_first_name)
            if self.company_second_name:
                provider.set('OtrosNombres', self.company_second_name)
        return provider

    def _get_qrcode(self):
        qrcode = element.CodigoQR(
            self.payroll.get_link_dian(cune=True, config=self.config))
        return qrcode

    def _get_general_information(self):
        version = "V1.0: Documento Soporte de Pago de Nómina Electrónica"
        if self.payroll_type == '103':
            version = "V1.0: Nota de Ajuste de Documento Soporte de Pago de Nómina Electrónica"
        information = element.InformacionGeneral(
            Version=version,
            Ambiente=str(self.environment),
            TipoXML=self.payroll_type,
            CUNE=self.payroll.get_cune(),
            EncripCUNE="CUNE-SHA384",
            FechaGen=self.issue_date,
            HoraGen=self.issue_time,
        )
        if self.payroll.type_note != '2':
            information.set('PeriodoNomina', self.period_payroll),
            information.set('TipoMoneda', self.currency)
            information.set('TRM', '0')
        return information

    def _get_notes(self):
        notes = element.Notas('')
        return notes

    def _get_information_company(self):
        information = element.Empleador(
            RazonSocial=self.company_full_name,
            NIT=str(self.company_id),
            DV=str(self.company_check_digit),
            Pais=str(self.company_country_code),
            DepartamentoEstado=str(self.company_department_code),
            MunicipioCiudad=str(
                self.company_department_code + self.company_city_code),
            Direccion=str(self.company_address),
        )
        if self.company_first_name:
            information.set('PrimerApellido', self.company_first_name)
            information.set('SegundoApellido', self.company_second_familyname)
            information.set('PrimerNombre', self.company_first_name)
            if self.company_second_name:
                information.set('OtrosNombres', self.company_second_name)
        return information

    def _get_information_employee(self):
        information = element.Trabajador(
            TipoTrabajador=str(self.type_employee),
            SubTipoTrabajador=str(self.subtype_employee),
            AltoRiesgoPension=str(self.alto_riesgo_pension).lower(),
            TipoDocumento=str(self.party_type_id),
            NumeroDocumento=str(self.party_id),
            PrimerApellido=self.employee_first_family_name,
            SegundoApellido=self.employee_second_family_name,
            PrimerNombre=self.employee_first_name,
            LugarTrabajoPais=str(self.work_place_country),
            LugarTrabajoDepartamentoEstado=str(self.work_place_department),
            LugarTrabajoMunicipioCiudad=str(
                self.work_place_department + self.work_place_city),
            LugarTrabajoDireccion=str(self.work_place_address),
            SalarioIntegral=str(self.integral_salary).lower(),
            TipoContrato=str(self.type_contract),
            Sueldo=str(self.salary),
        )
        if self.employee_second_name:
            information.set('OtrosNombres', self.employee_second_name),
        if self.code_employee:
            pass
            # information.set('CodigoTrabajador', str(self.code_employee)),
        return information

    def _get_payment_terms(self):
        information = element.Pago(
            Forma=self.payment_term,
            Metodo=self.payment_method,
        )
        if self.payroll.bank_payment:
            information.set('Banco', self.bank)
            information.set('TipoCuenta', self.bank_account_type)
            information.set('NumeroCuenta', self.bank_account)
        return information

    def _get_pay_date(self):
        pay_dates = element.FechasPagos(
            )
        for pay in self.payroll.payrolls_relationship:
            pay_dates.append(
                    element.FechaPago(str(pay.date_effective))
                )
        for pay in self.payroll.liquidations_relationship:
            pay_dates.append(
                    element.FechaPago(str(pay.liquidation_date))
                )
        return pay_dates

    def _get_predecessor(self):
        issue_date, issue_time = self.payroll.original_payroll.get_datetime_local()
        if self.payroll.type_note == '1':
            predecessor = element.ReemplazandoPredecesor()
        else:
            predecessor = element.EliminandoPredecesor()
        predecessor.set('NumeroPred', self.payroll.original_payroll.number)
        predecessor.set('CUNEPred', self.payroll.original_payroll.cune)
        predecessor.set('FechaGenPred', issue_date)
        return predecessor

    def _get_type_note(self):
        return element.TipoNota(self.payroll.type_note)

    def _get_lines(self):
        line_payments = []
        line_deductions = []
        for line in self.payroll.lines:
            if line.wage_type.definition == 'payment':
                line_payments.append(line)
            else:
                line_deductions.append(line)
        payments = self._get_payments(line_payments)
        deductions = self._get_deductions(line_deductions)

        return payments, deductions

    def _get_payments(self, line_payments):
        devengados = element.Devengados()
        subelements = {}
        for line in line_payments:
            concept = line.wage_type.type_concept_electronic
            if line_payments.index(line) == 0 and concept != 'Basico':
                basico = element.Basico(
                    DiasTrabajados="0",
                    SueldoTrabajado="0.0",
                )
                subelements['Basico'] = basico
            if concept == 'Basico':
                basico = element.Basico()
                factor = 1.0
                if line.uom.name == 'Hora':
                    factor = 8.0
                worked_days = line.quantity / Decimal(factor)
                basico.set('DiasTrabajados', rvalue(worked_days, 0))
                basico.set('SueldoTrabajado', rvalue(line.amount, 1))
                subelements['Basico'] = basico

            elif concept in WAGE_TYPE['Transporte']:
                if concept not in subelements.keys():
                    subelements['Transporte'] = element.Transporte()
                subelements['Transporte'].set(concept, rvalue(line.amount, 1))

            elif concept in WAGE_TYPE['HEDs']:
                subelements[concept] = element(concept + 's')
                hr = element(concept)
                hr.set('Cantidad', rvalue(line.quantity, 0))
                hr.set('Porcentaje', str(EXTRAS[concept]['percentaje']))
                hr.set('Pago', rvalue(line.amount, 2))
                subelements[concept].append(hr)
            elif concept in WAGE_TYPE['Vacaciones']:
                if 'Vacaciones' not in subelements.keys():
                    subelements['Vacaciones'] = element.Vacaciones()
                if concept == 'VacacionesComunes':
                    for l in line.lines_payroll:
                        quantity = Decimal(1.25 / 30) * l.quantity
                        e = element.VacacionesComunes(
                            FechaInicio=str(l.start_date),
                            FechaFin=str(l.end_date),
                            Cantidad=rvalue(quantity, 0),
                            Pago=rvalue(l.amount, 2)
                        )
                        subelements['Vacaciones'].append(e)
                else:
                    quantity = Decimal(1.25 / 30) * line.quantity
                    e = element.VacacionesCompensadas(
                        Cantidad=rvalue(quantity, 0),
                        Pago=rvalue(line.amount, 2)
                    )
                    subelements['Vacaciones'].append(e)

            elif concept in WAGE_TYPE['Primas']:
                if 'Primas' not in subelements.keys():
                    subelements['Primas'] = element.Primas()
                if concept == 'PrimasS':
                    subelements['Primas'].set(
                        'Cantidad', rvalue(line.quantity, 0))
                    subelements['Primas'].set('Pago', rvalue(line.amount, 2))
                else:
                    subelements['Primas'].set('PagoNs', rvalue(line.amount, 2))
            elif concept in WAGE_TYPE['Cesantias']:
                if 'Cesantias' not in subelements.keys():
                    subelements['Cesantias'] = element.Cesantias()
                if concept == 'Cesantias':
                    subelements['Cesantias'].set(
                        'Pago', rvalue(line.amount, 2))
                else:
                    subelements['Cesantias'].set('Porcentaje', '12.00')
                    subelements['Cesantias'].set(
                        'PagoIntereses', rvalue(line.amount, 2))

            elif concept in WAGE_TYPE['Incapacidades']:
                if 'Incapacidades' not in subelements.keys():
                    subelements['Incapacidades'] = element.Incapacidades()
                    for l in line.lines_payroll:
                        # line_payroll = l.line_payroll
                        e = element.Incapacidad()
                        e.set('FechaInicio', str(l.start_date))
                        e.set('FechaFin', str(l.end_date))
                        e.set('Cantidad', rvalue(l.quantity, 0))
                        e.set('Tipo', TIPO_INCAPACIDAD[concept])
                        e.set('Pago', rvalue(l.amount, 2))
                        subelements['Incapacidades'].append(e)

            elif concept in WAGE_TYPE['Licencias']:
                if 'Licencias' not in subelements.keys():
                    subelements['Licencias'] = element.Licencias()
                    for l in line.lines_payroll:
                        # line_payroll = l.line_payroll
                        e = element(concept)
                        e.set('FechaInicio', str(l.start_date))
                        e.set('FechaFin', str(l.end_date))
                        e.set('Cantidad', rvalue(l.quantity, 0))
                        if concept != 'LicenciaNR':
                            e.set('Pago', rvalue(l.amount, 2))
                        subelements['Licencias'].append(e)

            elif concept in WAGE_TYPE['Bonificaciones']:
                if 'Bonificaciones' not in subelements.keys():
                    subelements['Bonificaciones'] = element.Bonificaciones()
                    # subelements['Bonificaciones'].append(
                    #     element.Bonificacion())

                e = element('Bonificacion')
                e.set(concept, rvalue(line.amount, 2))
                subelements['Bonificaciones'].append(e)
                # if concept == 'BonificacionS':
                #     subelements['Bonificaciones'][0].set(
                #         concept, rvalue(line.amount, 2))
                # else:
                #     subelements['Bonificaciones'][0].set(
                #         concept, rvalue(line.amount, 2))

            elif concept in WAGE_TYPE['Auxilios']:
                if 'Auxilios' not in subelements.keys():
                    subelements['Auxilios'] = element.Auxilios()
                e = element('Auxilio')
                e.set(concept, rvalue(line.amount, 2))
                subelements['Auxilios'].append(e)

                # if concept == 'AuxilioS':
                #     subelements['Auxilios'][0].set(
                #         concept, rvalue(line.amount, 2))
                # else:
                #     subelements['Auxilios'][0].set(
                #         concept, rvalue(line.amount, 2))

            elif concept in WAGE_TYPE['HuelgasLegales']:
                if 'HuelgasLegales' not in subelements.keys():
                    subelements['HuelgaLegales'] = element.HuelgasLegales()

                e = element.HuelgaLegal(
                    FechaInicio="9999-12-31",
                    FechaFin="9999-12-31",
                    Cantidad="0"
                )

                subelements['HuelgaLegales'].append(e)

            elif concept in WAGE_TYPE['OtrosConceptos']:
                if 'OtrosConceptos' not in subelements.keys():
                    subelements['OtrosConceptos'] = element.OtrosConceptos()
                e = element.OtroConcepto()
                e.set('DescripcionConcepto', line.description)
                e.set(concept, rvalue(line.amount, 2))
                subelements['OtrosConceptos'].append(e)

            elif concept in WAGE_TYPE['Compensaciones']:
                if 'Compensaciones' not in subelements.keys():
                    subelements['Compensaciones'] = element.Compensaciones()

                e = element.Compensacion()
                e.set(concept, rvalue(line.amount, 2))
                subelements['Compensaciones'].append(e)

            elif concept in WAGE_TYPE['BonoEPCTVs']:
                if 'BonoEPCTVs' not in subelements.keys():
                    subelements['BonoEPCTVs'] = element.BonoEPCTVs()

                e = element.BonoEPCTV()
                e.set(concept, rvalue(line.amount, 2))
                subelements['BonoEPCTVs'].append(e)

            elif concept in WAGE_TYPE['OtrosTag']:
                e = element(concept)
                e.text = rvalue(line.amount, 2)

                if concept == 'Comision':
                    if concept + 'es' not in subelements.keys():
                        subelements[concept+'es'] = element(concept+'es')
                    subelements[concept + 'es'].append(e)
                else:
                    if concept + 's' not in subelements.keys():
                        subelements[concept+'s'] = element(concept+'s')
                    subelements[concept + 's'].append(e)

            elif concept in WAGE_TYPE['OtrosI']:
                e = element(concept)
                e.text = rvalue(line.amount, 2)
                subelements[concept] = e

        for e in subelements.values():
            devengados.append(e)
        return devengados

    def _get_deductions(self, line_deductions):
        deductions = element.Deducciones()
        subelements = {}

        deduccion_s = "0.0"
        porcentaje_s = "0.00"
        deduccion_p = "0.0"
        porcentaje_p = "0.00"
        for line in line_deductions:
            concept = line.wage_type.type_concept_electronic
            if concept == "Salud":
                deduccion_s = rvalue(line.amount, 2)
                porcentaje_s = '4.00'
            elif concept == "FondoPension":
                deduccion_p = rvalue(line.amount, 2)
                porcentaje_p = '4.00'
        salud = element.Salud(
            Porcentaje=porcentaje_s,
            Deduccion=deduccion_s,
        )
        subelements['Salud'] = salud
        pension = element.FondoPension(
            Porcentaje=porcentaje_p,
            Deduccion=deduccion_p,
        )
        subelements['FondoPension'] = pension

        for line in line_deductions:
            concept = line.wage_type.type_concept_electronic

            if concept in WAGE_TYPE['FondoSP']:
                if concept not in subelements.keys():
                    subelements['FondoSP'] = element.FondoSP()
                p = line.wage_type.unit_price_formula.split('*')
                p = Decimal(p[1].strip()) * 100
                if concept == 'FondoSP':
                    subelements['FondoSP'].set('Porcentaje', str(p))
                    subelements['FondoSP'].set(
                        'DeduccionSP', rvalue(line.amount, 2))
                else:
                    subelements['FondoSP'].set('PorcentajeSub', str(p))
                    subelements['FondoSP'].set(
                        'DeduccionSub', rvalue(line.amount, 2))
            elif concept in WAGE_TYPE['Sindicatos']:
                # WILSON ESTO FALLA PORQUE EL SINDICATO ESTA SIN FORMULA
                # PARECE QUE NO SIEMPRE ES CON %
                if concept not in subelements.keys():
                    subelements['Sindicatos'] = element.Sindicatos()
                p = line.wage_type.unit_price_formula.split('*')
                p = round(Decimal(p[1].strip()) * 100, 2)
                # p = '0'
                e = element.Sindicato(
                    Porcentaje=str(p),
                    Deduccion=rvalue(line.amount, 2)
                )
                subelements['Sindicatos'].append(e)
            elif concept in WAGE_TYPE['Sanciones']:
                if concept not in subelements.keys():
                    subelements['Sanciones'] = element.Sanciones(
                        element.Sancion())
                if concept == 'SancionPublic':
                    subelements['Sanciones'][0].set(
                        concept, rvalue(line.amount, 2))
                else:
                    subelements['Sanciones'][0].set(
                        concept, rvalue(line.amount, 2))
            elif concept in WAGE_TYPE['Libranzas']:
                if concept not in subelements.keys():
                    subelements['Libranzas'] = element.Libranzas()
                e = element.Libranza(
                    Descripcion=line.description,
                    Deduccion=rvalue(line.amount, 2)
                )
                subelements['Libranzas'].append(e)
            elif concept in WAGE_TYPE['OtrosTag']:
                if concept + 's' not in subelements.keys():
                    e = element(concept + 's')
                    subelements[concept + 's'] = e
                s = element(concept)
                s.text = rvalue(line.amount, 2)
                subelements[concept + 's'].append(s)
            elif concept == 'OtraDeduccion':
                if 'OtrasDeducciones' not in subelements.keys():
                    subelements['OtrasDeducciones'] = element.OtrasDeducciones()
                e = element.OtraDeduccion(rvalue(line.amount, 2))
                subelements['OtrasDeducciones'].append(e)
            elif concept in WAGE_TYPE['OtrosD']:
                e = element(concept)
                e.text = rvalue(line.amount, 2)
                subelements[concept] = e

        for e in subelements.values():
            deductions.append(e)
        return deductions

    def _get_total(self):

        DevengadosTotal = element.DevengadosTotal(
            rvalue(self.payroll.gross_payments, 2))
        DeduccionesTotal = element.DeduccionesTotal(
            rvalue(self.payroll.total_deductions, 2))
        ComprobanteTotal = element.ComprobanteTotal(
            rvalue(self.payroll.net_payment, 2))
        return DevengadosTotal, DeduccionesTotal, ComprobanteTotal

# ----------------------- MAKE ELECTRONIC payroll------------------------------

    def make(self, type):
        if type == '102':
            xml_payroll = self._get_head_psk()
            xml_payroll.append(self._get_payroll_period())
            xml_payroll.append(self._get_sequence())
            xml_payroll.append(self._get_place_generation())
            xml_payroll.append(self._get_provider())
            xml_payroll.append(self._get_qrcode())
            xml_payroll.append(self._get_general_information())
            xml_payroll.append(self._get_information_company())
            xml_payroll.append(self._get_information_employee())
            xml_payroll.append(self._get_payment_terms())
            xml_payroll.append(self._get_pay_date())
            acrueds, deductions = self._get_lines()
            xml_payroll.append(acrueds)
            xml_payroll.append(deductions)
            gross, deductions, net_payment = self._get_total()
            xml_payroll.append(gross)
            xml_payroll.append(deductions)
            xml_payroll.append(net_payment)

        elif type == '103':
            xml_payroll = self._get_credit_head_psk()
            type_note = self._get_type_note()
            xml_payroll.append(type_note)
            if self.payroll.type_note == '1':
                replace = element.Reemplazar()
                replace.append(self._get_predecessor())
                replace.append(self._get_payroll_period())
                replace.append(self._get_sequence())
                replace.append(self._get_place_generation())
                replace.append(self._get_provider())
                replace.append(self._get_qrcode())
                replace.append(self._get_general_information())
                # replace.append(self._get_notes())
                replace.append(self._get_information_company())
                replace.append(self._get_information_employee())
                replace.append(self._get_payment_terms())
                replace.append(self._get_pay_date())
                acrueds, deductions = self._get_lines()
                replace.append(acrueds)
                replace.append(deductions)
                gross, deductions, net_payment = self._get_total()
                replace.append(gross)
                replace.append(deductions)
                replace.append(net_payment)
                xml_payroll.append(replace)
            elif self.payroll.type_note == '2':
                delete = element.Eliminar()
                delete.append(self._get_predecessor())
                delete.append(self._get_sequence())
                delete.append(self._get_place_generation())
                delete.append(self._get_provider())
                delete.append(self._get_qrcode())
                delete.append(self._get_general_information())
                # delete.append(self._get_notes())
                delete.append(self._get_information_company())
                xml_payroll.append(delete)

        exml = etree.tostring(xml_payroll, encoding='UTF-8',
            pretty_print=True, xml_declaration=True)
        return exml
