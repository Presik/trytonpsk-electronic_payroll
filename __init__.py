from trytond.pool import Pool
from . import payroll
from . import wage_type
from . import configuration
from . import ir
from . import contract


def register():
    Pool.register(
        contract.Contract,
        wage_type.WageType,
        payroll.PayrollElectronic,
        payroll.PayrollElectronicLine,
        payroll.PayrollLineAccessLine,
        payroll.PayrollElectronicPayrollPeriod,
        payroll.PayrollElectronicLiquidationPeriod,
        # payroll.PayrollLineEvent,
        payroll.PayrollLineElectronicLine,
        configuration.Configuration,
        payroll.PayrollElectronicGroupStart,
        payroll.PayrollElectronicDetailedStart,
        ir.Cron,
        module='electronic_payroll', type_='model')
    Pool.register(
        payroll.PayrollElectronicGroup,
        payroll.PayrollElectronicMerge,
        payroll.PayrollElectronicDetailed,
        module='electronic_payroll', type_='wizard')
    Pool.register(
        payroll.PayrollElectronicReport,
        payroll.PayrollElectronicDetailedReport,
        module='electronic_payroll', type_='report')
