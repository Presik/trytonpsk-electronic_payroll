EXTRAS = {
    'HED': {'code': 1, 'percentaje': '25.00'},
    'HEN': {'code': 2, 'percentaje': '75.00'},
    'HRN': {'code': 3, 'percentaje': '35.00'},
    'HEDDF': {'code': 4, 'percentaje': '100.00'},
    'HRDDF': {'code': 5, 'percentaje': '75.00'},
    'HENDF': {'code': 6, 'percentaje': '150.00'},
    'HRNDF': {'code': 7, 'percentaje': '110.00'},
}


FISCAL_REGIMEN = {
    '48': 'RESPONSABLE DE IMPUESTO SOBRE LAS VENTAS – IVA',
    '49': 'NO RESPONSABLE DE IVA',
}

ENVIRONMENT = {
    '1': 'Produccion',
    '2': 'Pruebas',
}

KIND = {
    'steady': "1",
    'indefinite': "2",
    'job': "3",
    'learning': "4",
    'internships': "5",
}

TIPO_INCAPACIDAD = {
    'IncapacidadComun': '1',
    'IncapacidadProfesional': '2',
    'IncapacidadLaboral': '3',
}

ELECTRONIC_STATES = [
    ('none', 'None'),
    ('submitted', 'Submitted'),
    ('pending', 'Pending'),
    ('signed', 'Signed'),
    ('rejected', 'Rejected'),
    ('authorized', 'Authorized'),
    ('accepted', 'Accepted'),
]
PAYROLL_TYPE = [
    ('none', 'None'),
    ('102', 'Nomina Individual'),
    ('103', 'Nomina Individual De Ajuste'),
]

WAGE_TYPE = {
    'Basico': ['Basico'],
    'Transporte': ['AuxilioTransporte', 'ViaticoManuAlojS', 'ViaticoManuAlojNS'],
    'HEDs': ['HED', 'HEN', 'HRN', 'HEDDF', 'HRDDF', 'HENDF', 'HRNDF'],
    'Vacaciones': ['VacacionesComunes', 'VacacionesCompensadas'],
    'Primas': ['PrimasS', 'PrimasNS'],
    'Cesantias': ['Cesantias', 'IntCesantias'],
    'Incapacidades': ['IncapacidadComun', 'IncapacidadProfesional', 'IncapacidadLaboral'],
    'Licencias': ['LicenciaMP', 'LicenciaR', 'LicenciaNR'],
    'Bonificaciones': ['BonificacionS', 'BonificacionNS'],
    'Auxilios': ['AuxilioS', 'AuxilioNS'],
    'HuelgasLegales': ['HuelgaLegal'],
    'OtrosConceptos': ['ConceptoS', 'ConceptoNS'],
    'Compensaciones': ['CompensacionO', 'CompensacionE'],
    'BonoEPCTVs': ['PagoS', 'PagoNS', 'PagoAlimentacionS', 'PagoAlimentacionN'],
    'OtrosTag': ['Comision', 'PagoTercero', 'Anticipo'],
    'OtrosI': ['Dotacion', 'ApoyoSost', 'Teletrabajo', 'BonifRetiro', 'Indemnizacion', 'Reintegro'],
    'Salud': ['Salud'],
    'FondoPension': ['FondoPension'],
    'FondoSP': ['FondoSP', 'FondoSPSUB'],
    'Sindicatos': ['Sindicato'],
    'Sanciones': ['SancionPublic', 'SancionPriv'],
    'Libranzas': ['Libranza'],
    'OtrosD': ['PensionVoluntaria', 'RetencionFuente', 'AFC', 'Cooperativa', 'EmbargoFiscal', 'PlanComplementario', 'Educacion', 'Reintegro', 'Deuda']
}

MESSAGES = {
    'software_id': 'Falta ID del software Facturador',
    'software_pin': 'Falta PIN del software Facturador',
    'company_id': 'Falta numero NIT de la empresa',
    'company_full_name': 'Falta el nombre de la empresa',
    'company_check_digit': 'Falta el digito de verificacion de la empresa',
    'company_city_code': 'Falta la ciudad de la empresa',
    'company_address': 'Falta la direccion de la empresa',
    'company_country_code': 'Falta el pais de la empresa',
    'company_department_code': 'Falta el departamento de la empresa',
    'company_type_id': 'Falta el tipo de documento que identifica a la compañía',
    'work_place_department': 'Falta definir el departamento lugar de trabajo del empleado',
    'work_place_city': 'Falta definir el ciudad lugar de trabajo del empleado',
    'work_place_address': 'Falta definir el direccion lugar de trabajo del empleado',
    'type_contract': 'Debe definir el tipo de contrato',
    'salary': 'El contrato no tiene definido un salario',
    'payment_term': 'Falta el medio de pago',
    'payment_method': 'Falta el metodo de pago',
    'payroll_type': 'Falta definir el tipo de nomina',
    'environment': 'Debe definir el ambiente pruebas o produccion',
    'period_payroll': 'Falta definir el periodo de nomina',
    'prefix': 'Falta definir el prefijo de la secuencia',
    'employee_first_name': 'Falta el nombre del empleado',
    'party_id': 'Falta el id del cliente',
    'issue_date': 'Falta la fecha de factura'
}

PAYMENT_METHOD = [
    ('', ''),
    ('1', 'Instrumento no definido'),
    ('2', 'Crédito ACH'),
    ('3', 'Débito ACH'),
    ('4', 'Reversión débito de demanda ACH'),
    ('5', 'Reversión crédito de demanda ACH'),
    ('6', 'Crédito de demanda ACH'),
    ('7', 'Débito de demanda ACH'),
    ('8', 'Mantener'),
    ('9', 'Clearing Nacional o Regional'),
    ('10', 'Efectivo'),
    ('11', 'Reversión Crédito Ahorro'),
    ('12', 'Reversión Débito Ahorro'),
    ('13', 'Crédito Ahorro'),
    ('14', 'Débito Ahorro'),
    ('15', 'Bookentry Crédito'),
    ('16', 'Bookentry Débito'),
    ('17', 'Concentración de la demanda en efectivo/Desembolso Crédito (CCD)'),
    ('18', 'Concentración de la demanda en efectivo /Desembolso (CCD) débito'),
    ('19', 'Crédito Pago negocio corporativo (CTP)'),
    ('20', 'Cheque'),
    ('21', 'Proyecto bancario'),
    ('22', 'Proyecto bancario certificado'),
    ('23', 'Cheque bancario'),
    ('24', 'Nota cambiaria esperando aceptación'),
    ('41', 'Concentración efectivo/Desembolso Crédito plus (CCD+)'),
    ('42', 'Consignación bancaria'),
    ('43', 'Concentración efectivo / Desembolso Débito plus (CCD+)'),
    ('44', 'Nota cambiaria'),
    ('45', 'Transferencia Crédito Bancario'),
    ('46', 'Transferencia Débito Interbancario'),
    ('47', 'Transferencia Débito Bancaria'),
    ('48', 'Tarjeta Crédito'),
    ('49', 'Tarjeta Débito'),
    ('50', 'Postgiro'),
    ('51', 'Telex estándar bancario francés'),
    ('52', 'Pago comercial urgente'),
    ('53', 'Pago Tesorería Urgente'),
    ('60', 'Nota promisoria'),
    ('61', 'Nota promisoria firmada por el acreedor'),
    ('62', 'Nota promisoria firmada por el acreedor, avalada por el banco'),
    ('63', 'Nota promisoria firmada por el acreedor, avalada por un tercero '),
    ('64', 'Nota promisoria firmada por el banco'),
    ('65', 'Nota promisoria firmada por un banco avalada por otro banco'),
    ('66', 'Nota promisoria firmada'),
    ('67', 'Nota promisoria firmada por un tercero avalada por un banco'),
    ('70', 'Retiro de nota por el por el acreedor'),
    ('71', 'Bonos'),
    ('72', 'Vales'),
]
