# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Id
from trytond.transaction import Transaction


class Configuration(metaclass=PoolMeta):
    __name__ = "staff.configuration"

    payroll_electronic_sequence = fields.Many2One('ir.sequence.strict',
        'Electronic Payroll Sequence', domain=[
            ('sequence_type', '=',
                Id('staff_payroll', 'sequence_type_payroll')
            ), ['OR',
                ('company', '=', Eval('company')),
                ('company', '=', None),
            ],
        ], depends=['company'],
        help='This sequence must be with prefix and prefix only words')
    zip_electronic_sequence = fields.Many2One('ir.sequence',
        'Zip Electronic Sequence',
        domain=[
            ('sequence_type', '=',
                Id('electronic_payroll', 'sequence_type_zip_electronic')),
            ['OR',
                ('company', '=', Eval('company')),
                ('company', '=', None),
            ]
        ],
        depends=['company'], help='This sequence must be without prefix')
    pin_software = fields.Char('Pin Software')
    software_id = fields.Char('Software ID')
    environment = fields.Selection([
        ('', ''),
        ('1', 'Produccion'),
        ('2', 'Pruebas'),
        ], 'Environment')
    company = fields.Many2One('company.company', 'Company', required=True)
    period_payroll = fields.Selection([
        ('1', 'Semanal'),
        ('2', 'Decenal'),
        ('3', 'Catorcenal'),
        ('4', 'Quincenal'),
        ('5', 'Mensual'),
        ('6', 'Otro')
        ], 'Period Payroll')
    test_set_id = fields.Char('Test Set Id')
    send_method_api = fields.Selection([
        ('', ''),
        ('sincrono', 'Sincrono'),
        ('asincrono', 'Asincrono'),
        ], 'Send Method Api')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')
