''' Authentication functions for Stupendo Webservices '''
import base64
import json
import requests
import xmltodict
import zipfile
import os
from io import BytesIO
# from reportlab.pdfgen import canvas


# def xml_to_dict(xml):
#     root = ElementTree.XML(xml)
#     dict_res = XmlDictConfig(root)
#     return dict_res


def create_zip(payroll, xml_signed):
    memory_zip = BytesIO()
    with zipfile.ZipFile(memory_zip, 'x', allowZip64=True) as zf:
        zf.writestr(payroll.file_name_xml, xml_signed,
                    compress_type=zipfile.ZIP_DEFLATED)
        zf.close()
    return memory_zip.getvalue()


def encode_payroll(raw_xml, process='encode'):
    if process == 'encode':
        file64 = base64.b64encode(raw_xml)
        file64_decod = file64.decode('utf-8')
    else:
        file64 = base64.b64decode(raw_xml)
        file64_decod = file64.decode('utf-8')

    return file64_decod


def sing_file(payroll, url_sign):
    headers = {
        "Authorization": 'Bearer {}'.format(payroll.company.token_api),
        "Content-type": "application/json",
        "accept": "application/json",
    }
    file64_encod = encode_payroll(payroll.xml_payroll)

    params = {
      "xml_base64_bytes": file64_encod,
    }

    request = json.dumps(params)
    response = requests.post(url_sign, headers=headers, data=request)
    if response.status_code == 200:
        res = response.json()
        xml_signed = encode_payroll(res['xml_base64_bytes'], 'decode')
        payroll.xml_payroll = xml_signed.encode('utf8')
        payroll.electronic_state = 'signed'
        payroll.save()
        return response
    else:
        return {'result': 'false', 'message': 'Error al firmar el documento'}


def send_payroll_psk(payroll, config, api_send, url_sign):

    headers = {
        "Authorization": 'Bearer {}'.format(payroll.company.token_api),
        "Content-type": "application/json",
        "accept": "application/json",
    }
    response = sing_file(payroll, url_sign)
    if response.status_code == 200:
        res = response.json()
        xml_signed = encode_payroll(res['xml_base64_bytes'], 'decode')
        num_zip = config.zip_electronic_sequence.get()
        zip_file_name = 'z' + config.company.party.id_number.zfill(
            10) + payroll.start.strftime("%y") + hex(int(num_zip)).zfill(8) + '.zip'
        zip = create_zip(payroll, xml_signed)
        zip_base64 = encode_payroll(zip)
        if config.environment == '2':
            api_send = api_send + '/' + config.test_set_id
        else:
            api_send = api_send

        if config.send_method_api == 'sincrono':
            method = True
        else:
            method = False

        params = {
                  "sync": method,
                  "fileName": zip_file_name,
                  "contentFile": zip_base64,
        }
        request = json.dumps(params)
        response = requests.post(api_send, headers=headers, data=request)
        # print(response, response.text, 'sender: ' + api_send)
        if response.status_code == 200:
            return response.json()
        else:
            return {'result': 'false', 'message': 'Error al enviar documento a la Dian' + str(response)}


def send_request_status_psk(payroll, url_api, config):
    headers = {
        "Authorization": 'Bearer {}'.format(payroll.company.token_api),
        "Content-type": "application/json",
        "accept": "application/json",
    }
    if config and config.environment == '2':
        cufe = payroll.zip_key
    else:
        cufe = payroll.cune if payroll.cune else payroll.get_cune()
        if payroll.xml_response_dian_:
            # return False
            dict_res = xmltodict.parse(payroll.xml_response_dian_.decode('utf8'))
            response = dict_res['ApplicationResponse']['cac:DocumentResponse']
            if response['cac:Response']['cbc:ResponseCode'] in ['02', '04']:
                cufe = response['cac:DocumentReference']['cbc:UUID']['#text']
    url_api = url_api + cufe
    response = requests.post(url_api, headers=headers, data='')
    if response.status_code == 200:
        res = response.json()
        return res
    else:
        response = None
