#! -*- coding: utf8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelView, ModelSQL, Workflow
from decimal import Decimal
from itertools import chain
import calendar

from sql.operators import NotIn, NotEqual
from sql import Null
from datetime import date
from dateutil.relativedelta import relativedelta
import hashlib
import qrcode
from io import BytesIO

from trytond.pyson import Eval, Bool, Not, If, Or, Less, Len
from trytond.modules.company import CompanyReport
from trytond.transaction import Transaction
from trytond.report import Report
from trytond.pool import Pool
from trytond.wizard import (
    Wizard, StateView, StateTransition, Button, StateReport)
from trytond.exceptions import UserError
from .constants import EXTRAS, PAYMENT_METHOD, ELECTRONIC_STATES, PAYROLL_TYPE
from .wage_type import TYPE_CONCEPT_ELECTRONIC
from .it_supplier_psk import ElectronicPayrollPsk


_STATES = {
    'readonly': Eval('state') != 'draft',
}

USER = 'psk'

_DEPENDS = ['state']

STATES = [
    ('draft', 'Draft'),
    ('processed', 'Processed'),
    ('cancel', 'Cancel'),
    ('done', 'Done'),
]

TYPE_NOTE = [
    ('', ''),
    ('1', 'Reemplazar'),
    ('2', 'Eliminar'),
]


def rvalue(value):
    return str(round(abs(value), 2))


class PayrollElectronicLine(ModelSQL, ModelView):
    'Staff Payroll Electronic Line'
    __name__ = 'staff.payroll.electronic.line'
    sequence = fields.Integer('Sequence')
    payroll = fields.Many2One('staff.payroll.electronic', 'Payroll',
                              ondelete='CASCADE', select=True, required=True)
    description = fields.Char('Description', required=True)
    wage_type = fields.Many2One('staff.wage_type', 'Wage Type',
                                required=True, depends=['payroll'])
    uom = fields.Many2One('product.uom', 'Unit', depends=['wage_type'],
                          states={'readonly': Bool(Eval('wage_type'))})
    quantity = fields.Numeric('Quantity', digits=(16, 2))
    unit_value = fields.Numeric('Unit Value', digits=(16, 2))
    amount = fields.Numeric('Amount', digits=(16, 2), states={
            'readonly': ~Eval('_parent_payroll'),
        })
    party = fields.Many2One('party.party', 'Party', depends=['wage_type'])
    assitants_line = fields.Many2Many('payroll.electronic.line-access.line',
        'line', 'access_line', 'Hours Relationship', states={
            'readonly': ~Eval('_parent_payroll'),
        }, depends=['wage_type'])
    lines_payroll = fields.Many2Many('staff.payroll.line.electronic.line',
        'line_electronic', 'line_payroll', 'Lines Payroll',
            states={
                'readonly': ~Eval('_parent_payroll'),
            }, depends=['wage_type'])

    @classmethod
    def __setup__(cls):
        super(PayrollElectronicLine, cls).__setup__()
        cls._order.insert(0, ('sequence', 'ASC'))

    @staticmethod
    def default_quantity():
        return Decimal(str(1))


class PayrollElectronic(Workflow, ModelSQL, ModelView):
    'Staff Payroll Electronic'
    __name__ = 'staff.payroll.electronic'
    company = fields.Many2One('company.company', 'Company', required=True,
        states=_STATES, select=True, domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
            Eval('context', {}).get('company', -1)),
            ],
        depends=_DEPENDS)
    number = fields.Char('Number', readonly=True, help="Secuence", select=True)
    employee = fields.Many2One('company.employee', 'Employee',
        required=True, depends=['state'], select=True,
        states={
            'readonly': Eval('state') != 'draft',
            })
    start = fields.Date('Start Period', required=True, states={
            'readonly': Eval('state') != 'draft',
            })
    end = fields.Date('End Period', required=True, states={
            'readonly': Eval('state') != 'draft',
            })
    electronic_state = fields.Selection(ELECTRONIC_STATES, 'Electronic State')
    date_effective = fields.Date('Date Effective', states=_STATES,
        required=True)
    description = fields.Char('Description', states=_STATES, select=True)
    worked_days = fields.Integer('Worked Days', depends=[
        'start', 'end', 'state'])
    payroll_type = fields.Selection(PAYROLL_TYPE, 'Payroll Type')
    type_note = fields.Selection(TYPE_NOTE, 'Type Note', states={
            'invisible': Eval('payroll_type') != '103',
            'required': Eval('payroll_type') != '102',
            'readonly': Eval('state') != 'draft',
        }, depends=['payroll_type'])
    contract = fields.Many2One('staff.contract', 'Contract',
        select=True, domain=[
            ('employee', '=', Eval('employee')),
        ])
    state = fields.Selection(STATES, 'State')
    payment_method = fields.Selection(PAYMENT_METHOD, 'Payment Method')
    bank_payment = fields.Boolean('Bank Payment',
        help='check this option if the method of payment is by bank transfer bank ')
    cune = fields.Char('CUNE')
    provider_link = fields.Function(fields.Char('Link Mail', states={
            'invisible': Eval('electronic_state') != 'authorized',
        }), 'get_link_dian')
    lines = fields.One2Many('staff.payroll.electronic.line', 'payroll',
        'Wage Line', states={
            'invisible': Eval('type_note') == '2',
            'readonly': Eval('state') != 'draft'
            }, depends=['employee', 'state'])
    payrolls_relationship = fields.Many2Many('payroll.electronic.payroll.period',
        'payroll_electronic', 'payroll_periods', 'Payrolls Relationship')
    liquidations_relationship = fields.Many2Many('payroll.electronic.liquidation.period',
        'payroll_electronic', 'liquidation_periods', 'Liquidations Relationship')
    gross_payments = fields.Function(fields.Numeric('Gross Payments',
        digits=(16, 2), depends=['lines', 'states']), 'on_change_with_amount')
    total_deductions = fields.Function(fields.Numeric(
        'Total Deductions', digits=(16, 2), depends=['lines', 'states']),
        'on_change_with_amount')
    net_payment = fields.Function(fields.Numeric('Net Payment',
        digits=(16, 2), depends=['lines', 'states']), 'get_net_payment')
    net_payment_payroll = fields.Function(fields.Numeric('Net Payment Payroll'),
        'get_net_payment_payroll')
    currency = fields.Many2One('currency.currency', 'Currency',
        required=False, states={
            'readonly': ((Eval('state') != 'draft')
                        | (Eval('lines', [0]) & Eval('currency'))),
            }, depends=['state'])
    electronic_message = fields.Char('Electronic Message',
        states={'readonly': False})
    original_payroll = fields.Many2One('staff.payroll.electronic',
        'Original Payroll', domain=[
            ('employee', '=', Eval('employee')), ],
        states={
            'invisible': Not(Eval('payroll_type').in_(['103'])),
            'readonly': Eval('state') != 'draft'
            }, depends=['employee']
    )
    xml_payroll = fields.Binary('XML Payroll', readonly=True)
    file_name_xml = fields.Char('File Name')
    xml_response_dian_ = fields.Binary('XML Response Dian', readonly=True)
    zip_key = fields.Char('Zip Key', states={
        'invisible': Less(Len(Eval('zip_key')), 0),
        'readonly': True
        })
    qr_image = fields.Function(fields.Binary('QR Image'), 'get_qrcode_image')
    sended_mail = fields.Boolean('Sended Email', depends=['cune'])
    total_payable_amount = fields.Function(fields.Numeric(
        'Total Payable Amount', digits=(16, 2)), 'get_payable_amount')
    rules_fail = fields.Text('Rules Fail', states={
        'invisible': Eval('electronic_state') != 'rejected',
        'readonly': True
    }, depends=['electronic_state'])

    @classmethod
    def __setup__(cls):
        super(PayrollElectronic, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'cancel'),
            ('cancel', 'draft'),
            ('draft', 'processed'),
            ('processed', 'done'),
            ('done', 'draft'),
            ('processed', 'draft'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') == 'draft',
            },
            'do': {
                'invisible': Eval('state') != 'processed',
            },
            'cancel': {
                'invisible': Eval('state') != 'draft',
            },
            'process': {
                'invisible': Eval('state') != 'draft',
            },
            'submit': {
                'invisible': Or(
                    Eval('electronic_state') == 'authorized',
                    Eval('state') != 'processed',
                )},
            'force_response': {},
        })

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_start():
        Date = Pool().get('ir.date')
        today = Date.today() + relativedelta(months=-1)
        return date(today.year, today.month, 1)

    @staticmethod
    def default_end():
        Date = Pool().get('ir.date')
        today = Date.today() + relativedelta(months=-1)
        _, end_day = calendar.monthrange(today.year, today.month)
        return date(today.year, today.month, end_day)

    @staticmethod
    def default_electronic_state():
        return 'none'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_currency():
        Company = Pool().get('company.company')
        if Transaction().context.get('company'):
            company = Company(Transaction().context['company'])
        return company.currency.id

    @fields.depends('lines')
    def on_change_with_amount(self, name=None):
        res = []
        for line in self.lines:
            if not line.amount:
                continue
            if name == 'gross_payments':
                if line.wage_type.definition == 'payment':
                    res.append(line.amount)
            else:
                if line.wage_type.definition != 'payment' and line.wage_type.type_concept != 'unpaid_leave':
                    res.append(line.amount)
        res = self.currency.round(sum(res))
        return res

    @classmethod
    @ModelView.button
    def submit(cls, records):
        for payroll in records:
            if payroll.payroll_type is None:
                continue
            if payroll.validate_for_send():
                pool = Pool()
                Configuration = pool.get('staff.configuration')
                configuration = Configuration(1)
                _ = ElectronicPayrollPsk(payroll, configuration)
            else:
                payroll.get_message('Nomina no valida para enviar')
            if payroll.electronic_state == 'rejected':
                cls.force_response([payroll])

    @classmethod
    def send_payroll_cron(cls):
        payrolls = cls.search([
            ('state', '=', 'processed'),
            ('electronic_state', '!=', 'rejected'),
        ], limit=10)
        if not payrolls:
            payrolls = cls.search([
                ('state', '=', 'processed'),
                ('electronic_state', '=', 'rejected'),
            ], limit=10)
        for payroll in payrolls:
            try:
                cls.submit([payroll])
            except:
                pass

    @classmethod
    def delete(cls, records):
        # Cancel before delete
        cls.cancel(records)
        for payroll in records:
            if payroll.state != 'cancel':
                raise UserError(
                    'La nomina %s debe estar en estado borrador!', s=payroll.rec_name)
            if payroll.cune:
                raise UserError(
                    'No puede eliminar la nomina %s porque esta ya tiene cune', s=payroll.rec_name)
        super(PayrollElectronic, cls).delete(records)

    @classmethod
    def validate(cls, payrolls):
        super(PayrollElectronic, cls).validate(payrolls)
        for payroll in payrolls:
            pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('processed')
    def process(cls, records):
        for payroll in records:
            if payroll.type_note == '2' and payroll.lines:
                raise UserError(
                    'Nomina individual de ajuste con tipo de nota "Eliminar" no debe tener relacion de pagos')
            if payroll.payroll_type == '103' and payroll.original_payroll.electronic_state != 'authorized':
                raise UserError(
                    'Nomina original relacionada no esta autorizada')
            payroll.set_number()

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def do(cls, records):
        for payroll in records:
            pass

    @classmethod
    @ModelView.button
    def force_response(cls, records):
        for payroll in records:
            config = Pool().get('staff.configuration')(1)
            _ = ElectronicPayrollPsk.send_request_status_document(
                payroll, config)

    def get_net_payment_payroll(self, name):
        net_payment = 0
        for p in self.payrolls_relationship:
            net_payment += p.net_payment
        for l in self.liquidations_relationship:
            net_payment += l.net_payment
        return  net_payment

    def get_link_dian(self, name=None, cune=False, config=None):
        link_qr = 'https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey='
        if (config and config.environment == '2') or (self.zip_key and self.electronic_state == 'authorized'):
            link_qr = 'https://catalogo-vpfe-hab.dian.gov.co/document/searchqr?documentkey='
        if cune:
            link_qr = link_qr + str(self.get_cune())
        elif self.cune:
            link_qr = link_qr + self.cune
        return link_qr

    def set_number(self):
        if self.number:
            return
        pool = Pool()
        Configuration = pool.get('staff.configuration')
        configuration = Configuration(1)
        if not configuration.payroll_electronic_sequence:
            raise UserError(
                'Falta la secuencia de nomina electronica en configuracion')
        seq = configuration.payroll_electronic_sequence.get()
        self.write([self], {'number': seq})

    def get_message(self, message):
        raise UserError('message', message)

    def get_net_payment(self, name=None):
        return (self.gross_payments - self.total_deductions)

    def get_security_code(self, config):
        cadena = ''
        cadena = config.software_id + config.pin_software + self.number
        hash_ = hashlib.new("sha384", cadena.encode('utf8'))
        return hash_.hexdigest()

    def get_qrcode_image(self, name):
        qr_code = self.provider_link
        if qr_code:
            img = qrcode.make(qr_code)
            buffered = BytesIO()
            img.save(buffered, 'PNG')
            img_str = buffered.getvalue()
            return [img_str, 'image/png']
        else:
            return None

    def _get_payrolls_month(self):
        Payroll = Pool().get('staff.payroll')
        start = self.start
        end = self.end
        payrolls = Payroll.search([
            ('employee', '=', self.employee.id),
            ('start', '>=', start),
            ('start', '<=', end),
            ('contract', '=', self.contract.id),
            ('state', 'in', ['posted', 'processed']),
        ])
        return payrolls

    def _get_liquidations_month(self):
        Liquidation = Pool().get('staff.liquidation')
        start = self.start
        end = self.end
        liquidations = Liquidation.search([
            ('employee', '=', self.employee.id),
            ('liquidation_date', '>=', start),
            ('liquidation_date', '<=', end),
            ('contract', '=', self.contract.id),
            ('state', 'in', ['posted', 'confirmed']),
        ])
        return liquidations

    def get_cune(self):
        # Composición del CUNE = SHA‐384 (NumNE + FecNE + HorNE + ValDev + ValDed + ValTolNE + NitNE + DocEmp + TipoXML + Software‐Pin +TipAmb)
        cadena = ''
        inv_date, issue_time_ = self.get_datetime_local()
        pool = Pool()
        Configuration = pool.get('staff.configuration')
        config = Configuration(1)
        if config.pin_software and config.environment:
            val_dev = rvalue(self.gross_payments)
            val_ded = rvalue(self.total_deductions)
            val_net = rvalue(self.net_payment)
            nit_ne = self.company.party.id_number
            doc_emp = self.employee.party.id_number if self.type_note != '2' else '0'
            type_xml = self.payroll_type
            pin = config.pin_software
            amb = config.environment

            cadena = self.number + inv_date + issue_time_ + val_dev + val_ded \
                + val_net + nit_ne + doc_emp + type_xml + pin + amb
            hash_ = hashlib.new("sha384", cadena.encode('utf8'))
            return hash_.hexdigest()
        else:
            return self.get_message('Error al generar Cune')

    def get_datetime_local(self):
        create_date = self.company.convert_timezone(self.create_date)
        tzinfo = create_date.replace(microsecond=0)
        return str(tzinfo).split(' ')

    def _get_lines_extras(self, field=None):
        pool = Pool()
        Access = pool.get('staff.access')
        payrolls = self.payrolls_relationship
        value = Decimal('0.0')
        accesses = Access.search([('payroll', 'in', payrolls), ])
        for a in accesses:
            if getattr(a, field) == value:
                accesses.remove(a)
        return accesses

    def count_leap_years(self, d):
        years = d.year
        if (d.month <= 2):
            years -= 1
        return int(years / 4) - int(years / 100) + int(years / 400)

    def get_time_worked(self):
        contract = self.contract
        start_date = contract.start_date
        end_date = self.contract.finished_date \
            if self.contract.finished_date and \
            self.contract.finished_date < self.end else self.end
        # if contract.end_date:
        #     end_date = contract.end_date
        n1 = start_date.year * 360 + start_date.day
        for i in range(0, start_date.month - 1):
            n1 += 30
        n1 += self.count_leap_years(start_date)
        n2 = end_date.year * 360 + end_date.day
        for i in range(0, end_date.month - 1):
            n2 += 30
        n2 += self.count_leap_years(end_date)
        return (n2 - n1 + 1)

    def set_mergepayroll(self):
        pool = Pool()
        ElectronicPayrollLine = pool.get('staff.payroll.electronic.line')
        payrolls = self._get_payrolls_month()
        payrolls_lines = [list(p.lines)
                          for p in payrolls if hasattr(p, 'lines')]
        payrolls_lines = list(chain(*payrolls_lines))
        liquidations = self._get_liquidations_month()
        liquidations_lines = [list(p.lines)
                              for p in liquidations if hasattr(p, 'lines')]
        liquidations_lines = list(chain(*liquidations_lines))
        wage_to_create = {}
        worked_days = sum(
            [p.worked_days for p in payrolls if hasattr(p, 'worked_days')])
        self.worked_days = worked_days
        self.payrolls_relationship = payrolls
        self.liquidations_relationship = liquidations
        self.save()
        list_concepts = list(dict(TYPE_CONCEPT_ELECTRONIC).keys())
        for line in payrolls_lines:
            concept = line.wage_type.type_concept_electronic
            # Removed because now all concepts must be in electronic
            # payroll monthly without exceptions
            # _normal = line.wage_type.type_concept
            # wage_exceptions = [
            #     'interest', 'holidays', 'unemployment',
            #     'bonus_service', 'convencional_bonus'
            # ]
            if concept and line.quantity > 0:
                wage_id = line.wage_type.id
                sequence = list_concepts.index(concept)
                if wage_id not in wage_to_create:
                    wage_to_create[wage_id] = {
                        'payroll': self,
                        'sequence': sequence,
                        'wage_type': line.wage_type,
                        'description': line.description,
                        'quantity': line.quantity,
                        'unit_value': abs(line.unit_value),
                        'uom': line.wage_type.uom,
                        'amount': abs(line.amount),
                        'party': line.party,
                        'lines_payroll': [('add', [line.id, ])]
                    }
                    if concept in EXTRAS.keys():
                        field = line.description.split(' ')
                        field = field[0].lower()
                        relation_extras = self._get_lines_extras(field)
                        wage_to_create[wage_id].update(
                            {'assitants_line': [('add', relation_extras)]})
                else:
                    wage_to_create[wage_id]['lines_payroll'][0][1].append(
                        line.id)
                    wage_to_create[wage_id]['quantity'] += line.quantity
                    wage_to_create[wage_id]['amount'] += line.amount

        for liq_line in liquidations_lines:
            concept = liq_line.wage.type_concept_electronic
            amount = 0
            if concept:
                if liq_line.adjustments:
                    amount = sum(adj.amount for adj in liq_line.adjustments)
                elif not liq_line.move_lines and liq_line.amount:
                    amount = liq_line.amount

            if amount:
                days = liq_line.days or 0
                wage_id = liq_line.wage.id
                sequence = list_concepts.index(concept)
                if wage_id not in wage_to_create:
                    wage_to_create[wage_id] = {
                        'payroll': self,
                        'sequence': sequence,
                        'wage_type': liq_line.wage,
                        'description': liq_line.description,
                        'quantity': days,
                        'unit_value': abs(amount),
                        'uom': liq_line.wage.uom,
                        'amount': abs(amount),
                        'party': liq_line.party,
                    }
                else:
                    wage_to_create[wage_id]['quantity'] += days
                    wage_to_create[wage_id]['amount'] += abs(amount)
        ElectronicPayrollLine.create(wage_to_create.values())
        return 'end'

    def update_mergepayroll(self):
        pass

    def validate_for_send(self):
        return True

    def test_print(self):
        file_ = open("/home/" + USER + "/" + self.file_name_xml, "w")
        file_.write(self.xml_payroll.decode('utf-8'))
        file_.close()


class PayrollElectronicMerge(Wizard):
    'Payroll Electronic Merge'
    __name__ = 'staff.payroll_electronic.merge'
    start_state = 'do_merge'
    do_merge = StateTransition()

    def transition_do_merge(self):
        PayrollElectronic = Pool().get('staff.payroll.electronic')
        ids = Transaction().context['active_ids']

        for payroll in PayrollElectronic.browse(ids):
            if payroll.state != 'draft':
                return
            if not payroll.lines:
                payroll.set_mergepayroll()
            else:
                payroll.update_mergepayroll()
        return 'end'


class PayrollLineAccessLine(ModelSQL):
    "Payroll Line - AccessLine"
    __name__ = "payroll.electronic.line-access.line"
    _table = 'payroll_electronic_line_access_line_rel'
    line = fields.Many2One('staff.payroll.electronic.line', 'Line',
        ondelete='CASCADE', select=True, required=True)
    access_line = fields.Many2One('staff.access', 'Acess Line',
        ondelete='CASCADE', select=True, required=True)


class PayrollLineElectronicLine(ModelSQL):
    "Payroll Line"
    __name__ = "staff.payroll.line.electronic.line"
    _table = 'payroll_line_electronic_line_rel'
    line_electronic = fields.Many2One('staff.payroll.electronic.line', 'Line Electronic',
                                      ondelete='CASCADE', select=True)
    line_payroll = fields.Many2One('staff.payroll.line', 'Line Payroll',
                                   ondelete='CASCADE', select=True)


class PayrollElectronicPayrollPeriod(ModelSQL):
    "Payroll Electronic - Payroll Period"
    __name__ = "payroll.electronic.payroll.period"
    _table = 'payroll_electronic_payroll_period_rel'
    payroll_electronic = fields.Many2One('staff.payroll.electronic',
        'Payroll', ondelete='CASCADE', select=True, required=True)
    payroll_periods = fields.Many2One('staff.payroll', 'Payroll Periods',
        ondelete='CASCADE', select=True, required=True)


class PayrollElectronicLiquidationPeriod(ModelSQL):
    "Payroll Electronic - Payroll Period"
    __name__ = "payroll.electronic.liquidation.period"
    _table = 'payroll_electronic_liquidation_period_rel'
    payroll_electronic = fields.Many2One('staff.payroll.electronic', 'Payroll',
            ondelete='CASCADE', select=True, required=True)
    liquidation_periods = fields.Many2One('staff.liquidation', 'Payroll Periods',
            ondelete='CASCADE', select=True, required=True)


class PayrollElectronicReport(CompanyReport):
    "Payroll Electronic Report"
    __name__ = 'staff.payroll.electronic'

    @classmethod
    def get_context(cls, records, header, data):
        context = super().get_context(records, header, data)
        return context


class PayrollElectronicGroupStart(ModelView):
    'Payroll Electronic Group Start'
    __name__ = 'staff.payroll_electronic_group.start'
    date = fields.Date('Date', required=True,
        help='Seleciona cualquier fecha que este dentro del mes a reportar a la dian')
    payment_method = fields.Selection(PAYMENT_METHOD, 'Payment Method',
        required=True, help='Select the method of payment by which the employer made the payment to employees')
    bank_payment = fields.Boolean(
        'Bank Payment', help='confirm if payment is bank')
    description = fields.Char('Description', help='optional')
    company = fields.Many2One('company.company', 'Company', required=True)
    departments = fields.Many2Many('company.department', None, None,
            'Departments')
    employees = fields.Many2Many('company.employee', None, None, 'Employees')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class PayrollElectronicGroup(Wizard):
    'Payroll Electronic Group'
    __name__ = 'staff.payroll_electronic_group'
    start = StateView(
        'staff.payroll_electronic_group.start',
        'electronic_payroll.payroll_electronic_group_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Accept', 'open_', 'tryton-ok', default=True),
         ])
    open_ = StateTransition()

    def transition_open_(self):
        pool = Pool()
        Employee = pool.get('company.employee')
        PayrollElectronic = pool.get('staff.payroll.electronic')
        Contract = pool.get('staff.contract')
        _date = self.start.date
        _, end_day = calendar.monthrange(_date.year, _date.month)
        start_date = date(_date.year, _date.month, 1)
        end_date = date(_date.year, _date.month, end_day)
        #Remove employees with payroll this period
        payrolls_period = PayrollElectronic.search([
            ('start', '>=', start_date),
            ('start', '<=', end_date),
        ])

        contract_w_payroll = [p.contract.id for p in payrolls_period]
        dom_emp = [('active', '=', 'true'), ]
        if self.start.departments:
            dep = [d.id for d in self.start.departments]
            dom_emp.append(('department', 'in', dep))
        elif self.start.employees:
            emp = [d.id for d in self.start.employees]
            dom_emp.append(('id', 'in', emp))
        employees = Employee.search_read(dom_emp)
        employees = [d['id'] for d in employees]
        dom_contracts = self.get_contracts_dom(
            contract_w_payroll, employees, start_date, end_date)

        payroll_to_create = []
        contracts = [c[0] for c in dom_contracts]
        contracts = Contract.browse(contracts)
        for contract in contracts:
            values = self.get_values(contract, start_date, end_date)
            payroll_to_create.append(values)
        if payroll_to_create:
            payrolls = PayrollElectronic.create(payroll_to_create)
            if payrolls:
                for payroll in payrolls:
                    payroll.set_mergepayroll()
        return 'end'

    def get_contracts_dom(self, contract_w_payroll, employees, start_date, end_date):
        Payroll = Pool().get('staff.payroll')
        Liquidation = Pool().get('staff.liquidation')
        cursor = Transaction().connection.cursor()
        sql_payroll = Payroll.__table__()
        sql_liquidation = Liquidation.__table__()

        contract_ids = []
        if contract_w_payroll:
            where = (NotIn(sql_payroll.contract, contract_w_payroll))
        else:
            where = NotEqual(sql_payroll.contract, Null)
        cursor.execute(*sql_payroll.select(
            sql_payroll.contract,
            distinct=True,
            where=where
            & (sql_payroll.start >= start_date)
            & (sql_payroll.start <= end_date)
            & (sql_payroll.employee.in_(employees))))
        contract_ids.append(cursor.fetchall())

        if contract_w_payroll:
            where = (NotIn(sql_liquidation.contract, contract_w_payroll))
        else:
            where = NotEqual(sql_liquidation.contract, Null)
        cursor.execute(*sql_liquidation.select(
            sql_liquidation.contract,
            distinct=True,
            where=where
            & (sql_liquidation.liquidation_date >= start_date)
            & (sql_liquidation.liquidation_date <= end_date)
            & (sql_liquidation.employee.in_(employees))))
        contract_ids.append(cursor.fetchall())
        contract_ids = list(set(chain(*contract_ids)))
        return contract_ids

    def get_values(self, contract, start_date, end_date):
        Date = Pool().get('ir.date')
        date_effective = Date.today()
        employee = contract.employee
        values = {
            'employee': employee.id,
            'start': start_date,
            'end': end_date,
            'description': self.start.description,
            'payment_method': self.start.payment_method,
            'bank_payment': self.start.bank_payment,
            'date_effective': date_effective,
            'contract': contract.id,
            'payroll_type': '102',
        }
        return values


class PayrollElectronicDetailedStart(ModelView):
    'Payroll Electronic Detailed Start'
    __name__ = 'payroll.electronic_detailed.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    date = fields.Date('Date', required=True)
    departments = fields.Many2Many('company.department', None, None,
                                   'Departments')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class PayrollElectronicDetailed(Wizard):
    'Payroll Electronic Detailed'
    __name__ = 'payroll.electronic_detailed'

    start = StateView(
        'payroll.electronic_detailed.start',
        'electronic_payroll.payroll_electronic_detailed_start_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('payroll.electronic_detailed.report')

    def do_print_(self, action):
        departments = None
        if self.start.departments:
            departments = [d.id for d in self.start.departments]
        data = {
            'company': self.start.company.id,
            'date': self.start.date,
            'departments': departments,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class PayrollElectronicDetailedReport(Report):
    'Payroll Electronic Detailed Report'
    __name__ = 'payroll.electronic_detailed.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        PayrollElectronicLine = Pool().get('staff.payroll.electronic.line')

        _, end_day = calendar.monthrange(data['date'].year, data['date'].month)
        start_date = date(data['date'].year, data['date'].month, 1)
        end_date = date(data['date'].year, data['date'].month, end_day)
        dom = [
            ('payroll.company', '=', data['company']),
            ('payroll.start', '>=', start_date),
            ('payroll.end', '<=', end_date),
        ]

        if data['departments']:
            dom.append(
                [('payroll.employee.department', 'in', data['departments'])])

        fields_names = [
            'payroll.number', 'payroll.cune', 'payroll.start',
            'payroll.date_effective', 'payroll.payroll_type',
            'payroll.electronic_state', 'payroll.state',
            'wage_type.name', 'quantity', 'amount', 'payroll.end',
            'payroll.employee.party.id_number', 'payroll.employee.party.name',
            'payroll.gross_payments', 'payroll.net_payment',
            'payroll.total_deductions', 'payroll.payroll_type'
            ]
        payroll_lines = PayrollElectronicLine.search_read(
            dom, fields_names=fields_names)
        header_report = {li['wage_type.']['name'] for li in payroll_lines}
        records = {}
        default_payroll = cls.get_default_payroll(header_report)
        header_report = default_payroll.keys()
        total_report = default_payroll.copy()
        total_report['Numero'] = 'TOTAL'
        for line in payroll_lines:
            key = line['payroll.']['id']
            key_l = line['wage_type.']['name']
            try:
                records[key][key_l + '_C'] += line['quantity']
                records[key][key_l + '_V'] += line['amount']
                total_report[key_l+'_V'] += line['amount']
            except:
                payroll = default_payroll.copy()
                value = cls.get_value(line)
                payroll.update(value)
                payroll[key_l + '_C'] += line['quantity']
                payroll[key_l + '_V'] += line['amount']
                total_report[key_l+'_V'] += line['amount']
                total_report['Total Deducciones'] += value['Total Deducciones']
                total_report['Total Bruto'] += value['Total Bruto']
                total_report['Neto'] += value['Neto']
                records[key] = payroll

        report_context['records'] = records.values()
        report_context['header_report'] = header_report
        report_context['total_report'] = total_report.values()
        return report_context

    @classmethod
    def get_default_payroll(cls, header_report):
        payroll_dict = {
            'Numero': '',
            'Cune': '',
            'Tipo Nomina': '',
            'Estado': '',
            'Estado Electronico': '',
            'Fecha Inicio': '',
            'Fecha Fin': '',
            'Id_number': '',
            'Nombre': '',
            'Total Bruto': Decimal(0),
            'Total Deducciones': Decimal(0),
            'Neto': Decimal(0),

        }
        p_update = payroll_dict.update

        for h in header_report:
            p_update({h + '_C': Decimal(0), h+'_V': Decimal(0)})
        return payroll_dict

    @classmethod
    def get_value(cls, line):
        payroll = {
            'Numero': line['payroll.']['number'],
            'Cune': line['payroll.']['cune'],
            'Tipo Nomina': line['payroll.']['payroll_type'],
            'Estado': line['payroll.']['state'],
            'Estado Electronico': line['payroll.']['electronic_state'],
            'Fecha Inicio': line['payroll.']['start'],
            'Fecha Fin': line['payroll.']['end'],
            'Id_number': line['payroll.']['employee.']['party.']['id_number'],
            'Nombre': line['payroll.']['employee.']['party.']['name'],
            'Total Bruto': line['payroll.']['gross_payments'],
            'Total Deducciones': line['payroll.']['total_deductions'],
            'Neto': line['payroll.']['net_payment'],
        }
        return payroll
