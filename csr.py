import os
from OpenSSL import crypto
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa


# Generate our key
def generate_csr(file_name, company_name, department, city, web_domain, country_code='CO'):
    key = rsa.generate_private_key(
     public_exponent=65537,
     key_size=2048,
    )
    # Write our key to disk for safe keeping
    with open(get_location(company_name + '.key'), "wb") as f:
        f.write(key.private_bytes(
             encoding=serialization.Encoding.PEM,
             format=serialization.PrivateFormat.TraditionalOpenSSL,
             encryption_algorithm=serialization.BestAvailableEncryption(
                b"passphrase"),
        ))

    # Generate a CSR
    csr = x509.CertificateSigningRequestBuilder().subject_name(x509.Name([
         x509.NameAttribute(NameOID.COUNTRY_NAME, country_code),
         x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, department.upper()),
         x509.NameAttribute(NameOID.LOCALITY_NAME, city.upper()),
         x509.NameAttribute(NameOID.ORGANIZATION_NAME, company_name.upper()),
         x509.NameAttribute(NameOID.COMMON_NAME, web_domain),
         ])).add_extension(
             x509.SubjectAlternativeName([
                 # Describe what sites we want this certificate for.
                 x509.DNSName(web_domain),
                 # x509.DNSName('www.' + web_domain),
             ]),
             critical=False,
             # Sign the CSR with our private key.
             ).sign(key, hashes.SHA256())

    # Write our CSR out to disk.
    csr_file = None
    with open(get_location(file_name), "wb") as f:
        f.write(csr.public_bytes(serialization.Encoding.PEM))
        csr_file = f
    return csr_file, key


def get_location(file_name):
    file_ = os.path.join(os.path.abspath(
        os.path.dirname(__file__)), file_name)
    return file_


def converter_to_pfx(in_file, out_file, password):
    with open(in_file, "rb") as fin:
        pem_text = fin.read()
    print("Input length: {:d}".format(len(pem_text)))
    cert = crypto.load_certificate(crypto.FILETYPE_PEM, pem_text)

    print("CERTIFICATE DATA\n    Serial: 0x{:X}\n    Issuer: {:}\n    Subject: {:}\n    Not before: {:}\n    Not after: {:}".format(
            cert.get_serial_number(), cert.get_issuer(), cert.get_subject(), cert.get_notBefore(), cert.get_notAfter()))
    p12 = crypto.PKCS12()
    p12.set_certificate(cert)
    # p12_text = p12.export()
    p12_text = p12.export(passphrase=b"presiksas")
    print("Output length: {:d}".format(len(p12_text)))
    file_ = None
    out_file = get_location(out_file)
    with open(out_file, "wb") as fout:
        fout.write(p12_text)
        file_ = fout
    print("Done.")
    return file_
