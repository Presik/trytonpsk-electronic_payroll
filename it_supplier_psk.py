import xmltodict
from .builder_phase import ElectronicPayroll
from .response_supplier_ws import send_payroll_psk, encode_payroll, send_request_status_psk

API_SEND = {
    '1': 'https://presik.soenac.com/api/ubl2.1/send-payroll-async',
    '2': 'https://presik.soenac.com/api/ubl2.1/send-payroll-test-set-async',
    '3': 'https://presik.soenac.com/api/ubl2.1/status/zip/',
    '4': 'https://presik.soenac.com/api/ubl2.1/status/document/',
}
API_SIGN = {
    '103': 'https://presik.soenac.com/api/ubl2.1/signature/individual-payroll-adjustment',
    '102': 'https://presik.soenac.com/api/ubl2.1/signature/individual-payroll',
}


class ElectronicPayrollPsk(object):

    def __init__(self, payroll, config):
        self.payroll = payroll
        self.config = config
        self._create_electronic_payroll_phase()

    def _create_electronic_payroll_phase(self):
        ec_payroll = ElectronicPayroll(self.payroll, self.config)
        _type = 'nie'
        if self.payroll.payroll_type != '102':
            _type = 'niae'

        prefix = self.config.payroll_electronic_sequence.prefix
        seq = self.payroll.number.split(prefix, maxsplit=1)
        file_name = _type + \
            self.payroll.company.party.id_number.zfill(
                10) + hex(int(seq[1])).zfill(8) + '.xml'
        if ec_payroll.status != 'ok':
            self.payroll.get_message(ec_payroll.status)
        xml_payroll = ec_payroll.make(self.payroll.payroll_type)
        self.payroll.xml_payroll = xml_payroll
        self.payroll.file_name_xml = file_name
        self.payroll.save()
        # self.payroll.test_print()
        self._send_electronic_payroll()

    def _send_electronic_payroll(self):
        if 1:  # try:
            if self.config.environment == '2':
                api_send = API_SEND['2']
            else:
                api_send = API_SEND['1']

            res = send_payroll_psk(
                self.payroll,
                self.config,
                api_send,
                API_SIGN[self.payroll.payroll_type],
            )
            if res.get('result') and res.get('result') == 'false':
                self.payroll.get_message(res['message'])
                return
            if self.config.environment == '2':
                response = res['responseDian']['Envelope']['Body']
                if 'SendTestSetAsyncResponse' in response:
                    self.payroll.write([self.payroll], {
                        'electronic_state': 'submitted',
                        'cune': self.payroll.get_cune(),
                        'zip_key': response['SendTestSetAsyncResponse']['SendTestSetAsyncResult']['ZipKey']
                    })
            else:
                response = res['responseDian']['Envelope']['Body']['SendNominaSyncResponse']['SendNominaSyncResult']
                xml_response_dian = None
                if response.get('XmlBase64Bytes') and not isinstance(response['XmlBase64Bytes'], dict):
                    xml_response_dian = encode_payroll(
                        response['XmlBase64Bytes'], 'decode')
                if response['IsValid'] == 'true':

                    self.payroll.write([self.payroll], {
                        'cune': self.payroll.get_cune(),
                        'electronic_state': 'authorized',
                        'state': 'done',
                        'electronic_message': response['StatusMessage'],
                        'xml_response_dian_': xml_response_dian.encode('utf8') if xml_response_dian else None,
                    })
                else:
                    cadena = ''
                    error_msg = response['ErrorMessage']['string']
                    if isinstance(error_msg, list):
                        for r in error_msg:
                            cadena += r + '\n'
                    else:
                        cadena = error_msg
                    self.payroll.write([self.payroll], {
                        'rules_fail': cadena,
                        'electronic_message': response['StatusMessage'],
                        'electronic_state': 'rejected',
                        'xml_response_dian_': xml_response_dian.encode('utf8') if xml_response_dian else None,
                    })

        else:  # except Exception as e:
            self.payroll.get_message('Error de Envio')

    def send_request_status_document(payroll, config):
        api_send = API_SEND['3']
        if config and config.environment == '1':
            api_send = API_SEND['4']
        res = send_request_status_psk(
            payroll,
            api_send,
            config,
        )
        if res is None:
            payroll.get_message('Error de solicitud')
        if res is False:
            return
        if config and config.environment == '1':
            response = res['responseDian']['Envelope']['Body']['GetStatusResponse']['GetStatusResult']
        else:
            response = res['responseDian']['Envelope']['Body']['GetStatusZipResponse']['GetStatusZipResult']['DianResponse']
        if response['IsValid'] == 'true':
            xml_response_dian = None
            if response.get('XmlBase64Bytes') and not isinstance(response['XmlBase64Bytes'], dict):
                xml_response_dian = encode_payroll(
                    response['XmlBase64Bytes'], 'decode')
            payroll.write([payroll], {
                'cune': response['XmlDocumentKey'],
                'electronic_state': 'authorized',
                'state': 'done',
                'electronic_message': response['StatusMessage'],
                'xml_response_dian_': xml_response_dian.encode('utf8'),
            })
        elif response['StatusCode'] == '99':
            xml_response_dian = None
            if response.get('XmlBase64Bytes') and not isinstance(response['XmlBase64Bytes'], dict):
                xml_response_dian = encode_payroll(
                    response['XmlBase64Bytes'], 'decode')
                dict_res = xmltodict.parse(xml_response_dian)
                response = dict_res['ApplicationResponse']['cac:DocumentResponse']
                if response['cac:Response']['cbc:ResponseCode'] in ['02', '04']:
                    uuid = response['cac:DocumentReference']['cbc:UUID']['#text']
                    cune = payroll.get_cune()
                    if cune == uuid:
                        electronic_message = response['cac:LineResponse'][1]['cac:Response']['cbc:Description']
                        payroll.write([payroll], {
                            'cune': cune,
                            'electronic_state': 'authorized',
                            'state': 'done',
                            'electronic_message': electronic_message,
                            'xml_response_dian_': xml_response_dian.encode('utf8'),
                        })
        else:
            cadena = ''
            error_msg = response['ErrorMessage'].get('string')
            if isinstance(error_msg, list):
                for r in response['ErrorMessage']['string']:
                    cadena += r + '\n'
            else:
                cadena = error_msg
            payroll.write([payroll], {
                'rules_fail': cadena,
                'electronic_message': response['StatusMessage'],
                'electronic_state': 'rejected',
            })
        return
