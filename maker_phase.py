from lxml import builder, etree

attr_qname = etree.QName('http://www.w3.org/2001/XMLSchema-instance', 'schemaLocation')
namespaces = {
    None: 'dian:gov:co:facturaelectronica:NominaIndividual',
    'xs': 'http://www.w3.org/2001/XMLSchema-instance',
    'ds': 'http://www.w3.org/2000/09/xmldsig#',
    'ext': 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2',
    'xades': 'http://uri.etsi.org/01903/v1.3.2#',
    'xades141': 'http://uri.etsi.org/01903/v1.4.1#',
    'xsi': "http://www.w3.org/2001/XMLSchema-instance",
}
attr_xsi = "dian:gov:co:facturaelectronica:NominaIndividual NominaIndividualElectronicaXSD.xsd"

Root_psk = builder.ElementMaker(nsmap=namespaces)

# ----------------------Nomina Individual De Ajuste ---------------------------
namespaces = {
    None: 'dian:gov:co:facturaelectronica:NominaIndividualDeAjuste',
    'xs': 'http://www.w3.org/2001/XMLSchema-instance',
    'ds': 'http://www.w3.org/2000/09/xmldsig#',
    'ext': 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2',
    'xades141': 'http://uri.etsi.org/01903/v1.4.1#',
    'xades': 'http://uri.etsi.org/01903/v1.3.2#',
    'xsd': 'http://www.w3.org/2001/XMLSchema',

}
Root_psk_na = builder.ElementMaker(nsmap=namespaces)
attr_xsi_na = "dian:gov:co:facturaelectronica:NominaIndividualDeAjuste NominaIndividualDeAjusteElectronicaXSD.xsd"

ext = builder.ElementMaker(namespace=namespaces['ext'])
element = builder.ElementMaker(namespace=None)
ds = builder.ElementMaker(namespace=namespaces['ds'])
